package com.example.chandan.codingchallenge;

import com.example.chandan.codingchallenge.model.JsonData;

/**
 * Contract class which act a glue between view and the presenter.
 */

public class MainContract {

    public interface View {
        void showData(JsonData jsonData);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadData();
    }
}
