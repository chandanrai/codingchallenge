package com.example.chandan.codingchallenge;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.chandan.codingchallenge.model.Row;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.chandan.codingchallenge.R.id;
import static com.example.chandan.codingchallenge.R.layout;

/**
 * Adapter class for binding the row data to the recycler view.
 */
public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Row> rows;
    private Context context;

    Adapter(Context context) {
        this.rows = new ArrayList<>();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(layout.item_row, parent, false);
        return new RowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Row row = rows.get(position);
        RowViewHolder rowViewHolder = (RowViewHolder) holder;
        resetVisibility(rowViewHolder);

        if (row.getTitle() != null) {
            rowViewHolder.title.setText(row.getTitle());
        } else {
            rowViewHolder.title.setVisibility(View.GONE);
        }

        if (row.getDescription() != null) {
            rowViewHolder.description.setText(row.getDescription());
        } else {
            rowViewHolder.description.setVisibility(View.GONE);
        }

        if (row.getImageHref() != null) {
            Glide.with(context)
                    .load(row.getImageHref())
                    .into(rowViewHolder.imageView);
        } else {
            rowViewHolder.imageView.setVisibility(View.GONE);
        }
    }

    private void resetVisibility(RowViewHolder holder) {
        holder.title.setVisibility(View.VISIBLE);
        holder.description.setVisibility(View.VISIBLE);
        holder.imageView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return rows == null ? 0 : rows.size();
    }

    void addRows(List<Row> list) {
        this.rows.addAll(list);
        notifyDataSetChanged();
    }

    void clearData() {
        this.rows.clear();
    }

    /**
     * This inner class is used a View holder for row list items.
     */
    class RowViewHolder extends RecyclerView.ViewHolder {
        @BindView(id.text_view_title)
        TextView title;

        @BindView(id.text_view_description)
        TextView description;

        @BindView(id.image_view)
        ImageView imageView;

        RowViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setTag(this);
        }
    }
}
