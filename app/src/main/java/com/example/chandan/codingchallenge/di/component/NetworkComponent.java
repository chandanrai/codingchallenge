package com.example.chandan.codingchallenge.di.component;

import com.example.chandan.codingchallenge.MainActivity;
import com.example.chandan.codingchallenge.di.module.ApplicationModule;
import com.example.chandan.codingchallenge.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by chandan on 9/4/18.
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface NetworkComponent {
    Retrofit retrofit();
}
