package com.example.chandan.codingchallenge;


import com.example.chandan.codingchallenge.model.JsonData;

import javax.inject.Inject;

import retrofit2.Retrofit;
import retrofit2.http.GET;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by chandan on 10/4/18.
 */

public class MainPresenter implements MainContract.Presenter {

    private Retrofit retrofit;
    private MainContract.View view;

    @Inject
    public MainPresenter(Retrofit retrofit, MainContract.View view) {
        this.retrofit = retrofit;
        this.view = view;
    }

    @Override
    public void loadData() {
        retrofit.create(ApiService.class).getJsonData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<JsonData>() {
                    @Override
                    public void onCompleted() {
                        view.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(JsonData data) {
                        view.showData(data);
                    }
                });

    }

    /**
     * Retrofit interface to get Json data.
     */
    public interface ApiService {
        @GET("facts.json")
        Observable<JsonData> getJsonData();
    }
}
