package com.example.chandan.codingchallenge;

import android.app.Application;

import com.example.chandan.codingchallenge.di.component.DaggerNetworkComponent;
import com.example.chandan.codingchallenge.di.component.NetworkComponent;
import com.example.chandan.codingchallenge.di.module.ApplicationModule;
import com.example.chandan.codingchallenge.di.module.NetworkModule;

/**
 * Created by chandan on 9/4/18.
 */

public class CodingChallengeApplication extends Application {

    private NetworkComponent networkComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        networkComponent = DaggerNetworkComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }
}
