package com.example.chandan.codingchallenge.di.module;

import com.example.chandan.codingchallenge.MainContract;
import com.example.chandan.codingchallenge.di.PerActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final MainContract.View view;

    public ActivityModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    MainContract.View providesMainContractView() {
        return view;
    }
}
