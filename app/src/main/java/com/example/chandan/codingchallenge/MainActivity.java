package com.example.chandan.codingchallenge;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.chandan.codingchallenge.di.component.DaggerActivityComponent;
import com.example.chandan.codingchallenge.di.module.ActivityModule;
import com.example.chandan.codingchallenge.model.JsonData;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Applications main activity which displays Json data as list of rows and data can be refreshed
 * by using pull to refresh.
 */
public class MainActivity extends AppCompatActivity implements MainContract.View, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    MainPresenter presenter;
    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaggerActivityComponent.builder()
                .networkComponent(((CodingChallengeApplication) getApplicationContext()).getNetworkComponent())
                .activityModule(new ActivityModule(this))
                .build().inject(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        adapter = new Adapter(this);
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setColorSchemeColors(Color.BLUE);

        presenter.loadData();
    }

    @Override
    public void showData(JsonData jsonData) {
        swipeRefreshLayout.setRefreshing(false);
        adapter.clearData();
        adapter.addRows(jsonData.getRows());
        getSupportActionBar().setTitle(jsonData.getTitle());
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, getString(R.string.error_message), Toast.LENGTH_SHORT).show();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showComplete() {

    }

    @Override
    public void onRefresh() {
        presenter.loadData();
    }
}
