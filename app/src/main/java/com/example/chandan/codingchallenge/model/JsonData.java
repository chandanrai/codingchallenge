package com.example.chandan.codingchallenge.model;

import java.util.List;

/**
 * Model class for Json data
 */

public class JsonData {
    private String title;
    private List<Row> rows;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
}
