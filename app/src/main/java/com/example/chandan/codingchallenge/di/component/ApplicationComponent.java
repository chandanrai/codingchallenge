package com.example.chandan.codingchallenge.di.component;

import android.app.Application;

import com.example.chandan.codingchallenge.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    Application application();
}
