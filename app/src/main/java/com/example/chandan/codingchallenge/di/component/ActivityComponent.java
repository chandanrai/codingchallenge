package com.example.chandan.codingchallenge.di.component;

import com.example.chandan.codingchallenge.MainActivity;
import com.example.chandan.codingchallenge.di.PerActivity;
import com.example.chandan.codingchallenge.di.module.ActivityModule;

import dagger.Component;
import dagger.Subcomponent;


/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Component(dependencies = NetworkComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
}
